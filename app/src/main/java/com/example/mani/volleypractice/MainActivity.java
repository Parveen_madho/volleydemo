package com.example.mani.volleypractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


EditText Authentication,DeviceID;
    Button click;
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Authentication=(EditText) findViewById(R.id.txt_key);
        DeviceID=(EditText) findViewById(R.id.txt_id);
        click=(Button) findViewById(R.id.btn);


        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registerUser();


            }
        });


    }
    private void registerUser(){
        final String username = Authentication.getText().toString().trim();
        final String password = DeviceID.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://newadmin.ciaoim.com/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(KEY_USERNAME,username);
                params.put(KEY_PASSWORD,password);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
